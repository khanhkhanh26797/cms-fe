export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user/login', name: 'login', component: './User/Login', },
      { component: '404', },
    ],
  },

  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      { path: '/', redirect: '/Topic', },
      // { path: '/Department', component: './Department', name: 'Phòng ban', authority: ['staff','teacher'], }, 
      { path: '/StudentList', component: './StudentList', name: 'Student List', authority: ['staff'], },
      { path: '/Courses', component: './Courses', name: 'Courses', authority: ['staff'], },
      { path: '/Topic', component: './Topic', name: 'Topic', authority: ['staff','teacher'], },
      { path: '/TeacherList', component: './TeacherList', name: 'Teacher List', authority: ['staff'], },
      { path: '/AccountInfo', component: './AccountInfo', name: 'Account Info', authority: ['teacher'], },

      { component: '404', },
    ],
  },
];
