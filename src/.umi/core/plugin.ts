// @ts-nocheck
import { Plugin } from '/Volumes/Data/cms-frontend/node_modules/@umijs/runtime';

const plugin = new Plugin({
  validKeys: ['modifyClientRenderOpts','patchRoutes','rootContainer','render','onRouteChange','dva','getInitialState','locale','locale','request',],
});

export { plugin };
