// @ts-nocheck
import React from 'react';
import { ApplyPluginsType, dynamic } from '/Volumes/Data/cms-frontend/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';
import LoadingComponent from '@/components/PageLoading/index';

export function getRoutes() {
  const routes = [
  {
    "path": "/user",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__UserLayout' */'/Volumes/Data/cms-frontend/src/layouts/UserLayout'), loading: LoadingComponent}),
    "routes": [
      {
        "path": "/user/login",
        "name": "login",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__User__Login' */'/Volumes/Data/cms-frontend/src/pages/User/Login'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/Volumes/Data/cms-frontend/src/pages/404'), loading: LoadingComponent}),
        "exact": true
      }
    ]
  },
  {
    "path": "/",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__BasicLayout' */'/Volumes/Data/cms-frontend/src/layouts/BasicLayout'), loading: LoadingComponent}),
    "Routes": [
      "src/pages/Authorized"
    ],
    "routes": [
      {
        "path": "/",
        "redirect": "/Topic",
        "exact": true
      },
      {
        "path": "/StudentList",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__StudentList' */'/Volumes/Data/cms-frontend/src/pages/StudentList'), loading: LoadingComponent}),
        "name": "Student List",
        "authority": [
          "staff"
        ],
        "exact": true
      },
      {
        "path": "/Courses",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__Courses' */'/Volumes/Data/cms-frontend/src/pages/Courses'), loading: LoadingComponent}),
        "name": "Courses",
        "authority": [
          "staff"
        ],
        "exact": true
      },
      {
        "path": "/Topic",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__Topic' */'/Volumes/Data/cms-frontend/src/pages/Topic'), loading: LoadingComponent}),
        "name": "Topic",
        "authority": [
          "staff",
          "teacher"
        ],
        "exact": true
      },
      {
        "path": "/TeacherList",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__TeacherList' */'/Volumes/Data/cms-frontend/src/pages/TeacherList'), loading: LoadingComponent}),
        "name": "Teacher List",
        "authority": [
          "staff"
        ],
        "exact": true
      },
      {
        "path": "/AccountInfo",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__AccountInfo' */'/Volumes/Data/cms-frontend/src/pages/AccountInfo'), loading: LoadingComponent}),
        "name": "Account Info",
        "authority": [
          "teacher"
        ],
        "exact": true
      },
      {
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/Volumes/Data/cms-frontend/src/pages/404'), loading: LoadingComponent}),
        "exact": true
      }
    ]
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
