import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option } from 'antd';
import { getAllStudents, createStudentInfo, editStudentInfo, deleteStudentInfo, createUsers, getUsersDetails, editUsers, deleteUsers } from '@/services/StudenList';
import { getAllCourses, createCourses, getCoursesDetails, editCourses, deleteCourses } from '@/services/Courses';

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

function CustomTextInput(props) {
  return (
    <div>
      <input ref={props.inputRef} />
    </div>
  );
}

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
  }

  return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.columns = [
      {
        title: 'Course Name',
        dataIndex: 'courseName',
        key: 'id',
      },
      {
        title: 'Category',
        dataIndex: 'category',
        key: 'id',
      },
      // {
      //   title: 'List Topic Details',
      //   dataIndex: 'edit',
      //   render: (text, record) =>
      //     this.state.dataSource.length >= 1 ? (
      //       <Button
      //         onClick={() => this.handShow(record)}
      //         type="primary"
      //         style={{
      //           marginBottom: 16,
      //         }}
      //       >
      //         Show
      //     </Button>
           
      //     ) : null,
      // },
      {
        title: 'Edit',
        dataIndex: 'edit',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            
            <Popconfirm title="Sure?" onConfirm={() => this.handEdit(record.id)}>
              <a>Edit</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Delete',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      form: [],
      dataSource: [],
      count: 2,
      modalVisible: false,
      modalVisibleEdit: false,
      dataModalEdit: null
    };

  }

  componentDidMount = async () => {
    await this.getData()
  }

  addItem = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };
  
  getData = async () => {
    const result = await getAllCourses();
    console.log(result)
    this.setState({ dataSource: result })
  }
  getCoursesDetails = async (id) => {
    const result = await getCoursesDetails(id);
    console.log(result)
    this.setState({ dataModalEdit: result })
  }
  createCourses = async (values) => {
    console.log('Success:', values);
    let body = {
      "courseName": values.courseName,
      "category": values.category,
    }
    const result = await createCourses(body);
    console.log(result)
    await this.getData()
    this.setState({ modalVisible: false })
  };

  editCourses = async (values) => {
    let id = this.state.dataModalEdit.id
    console.log(id)
    console.log('Success:', values);
    let body = {
      "courseName": values.courseName,
      "category": values.category,
    }
    const result = await editCourses(body, this.state.dataModalEdit.id);
    console.log(result)
    await this.getData()
    this.setState({ modalVisibleEdit: false })
  };

  handleDelete = async (record) => {
    const result1 = await deleteCourses(record.id);
    console.log(result1)
    this.getData()
  };
  handEdit = async (id) => {
    console.log(id)
    await this.getCoursesDetails(id)
    this.setState({ modalVisibleEdit: true })
  };
  handShow = async (record) => {
    console.log(record)
  };
  
  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  render() {

    console.log(this.state.dataModalEdit)
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>
        <PageHeader
          ghost={false}
          title={`Student List`}
          extra={[
            <Button
              onClick={() => this.addItem()}
              type="primary"
              style={{
                marginBottom: 16,
              }}
            >
              Add Coures
          </Button>,
            <Button
              onClick={() => { this.getData() }}
              type="primary"
              style={{
                marginBottom: 16,
              }}

            >
              Reload
            </Button>,
          ]}
        ></PageHeader>

        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
        <Modal
          destroyOnClose
          title="Add Coures"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              onFinish={this.createCourses}
            >
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Coures Name ',
                  },
                ]}
                label="Coures Name" name="courseName" >
                <Input placeholder="Enter Coures Name" />
              </Form.Item>

              <Form.Item
                name="category"
                label="Category"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Category ',
                  },
                ]}
              >
                <Select placeholder="Category">
                  <Select value="IT">Information Technology</Select>
                  <Select value="Business">Business</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.closeModal()}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>

          ]}
        >
        </Modal>,
        <Modal
          destroyOnClose
          title="Edit Course Info"
          visible={this.state.modalVisibleEdit}
          onCancel={() => this.setState({ modalVisibleEdit: false })}
          footer={[

            <Form
              onFinish={this.editCourses}
            >
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Coures Name ',
                  },
                ]}
                label="Coures Name" name="courseName" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.courseName}>
                <Input placeholder="Coures Name" />
              </Form.Item>

              <Form.Item
                name="category"
                label="Category"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Category ',
                  },
                ]}
                hasFeedback
                initialValue={this.state.dataModalEdit && this.state.dataModalEdit.category}
              >
                <Select placeholder="Category">
                  <Select value="IT">Information Technology</Select>
                  <Select value="Business">Business</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>
          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
