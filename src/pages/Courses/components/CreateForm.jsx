import React from 'react';
import { Modal, Form, Input, Select, Row, Col, Button } from 'antd';
const CreateForm = (props) => {
  // const { getInfo } = form;
  const { modalVisible, onCancel } = props;
  const createItem = () => {
    alert("Created")
  };
  return (
    <Modal
      destroyOnClose
      title="Thêm mới nhân viên"
      visible={modalVisible}
      onCancel={() => onCancel()}
      footer={null}
      footer={[
        <Button
          key="submit"
          type="primary"
          onClick={() => createItem()}
        >
          Thêm
      </Button>,
        <Button key="back" onClick={() => onCancel()}>
          Huỷ
        </Button>,

      ]}
    >
      <Row gutter={24}>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Name">
          {/* {getInfo('name', {
              initialValue: product.name,
              rules: [{ required: true, message: 'Please fill in product name' }],
            })(<Input placeholder="Product name" />)} */}
          </Form.Item>
        </Col>
      </Row>
    </Modal>
  );
};

export default CreateForm;
