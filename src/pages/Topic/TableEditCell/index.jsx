import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option, List,Link } from 'antd';
import { getAllTopic, createTopic, editTopic, deleteTopic, getTopicDetails, getTopicOfTeacher, getAllTeacher } from '@/services/Topic';

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

function CustomTextInput(props) {
  return (
    <div>
      <input ref={props.inputRef} />
    </div>
  );
}

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
  }

  return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.columns = [
      {
        title: 'Topic Name',
        dataIndex: 'subName',
        key: 'id',
      },
      {
        title: 'Topic code',
        dataIndex: 'subID',
        key: 'id',
      },
      {
        title: 'Edit',
        dataIndex: 'edit',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure?" onConfirm={() => this.handEdit(record.id)}>
              <a >Edit</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Delete',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      form: [],
      dataSource: [],
      count: 2,
      modalVisible: false,
      modalVisibleEdit: false,
      modalListTeacher: false,
      dataModalEdit: null,
      listTeacher: [],
      allTeacher: [],
      teacherNameList: ["khanh","khanh1",],
      columns: [
        {
          title: 'Topic Name',
          dataIndex: 'subName',
          key: 'id',
        },
        {
          title: 'Topic code',
          dataIndex: 'subID',
          key: 'id',
        },
        {
          title: 'List Teacher',
          dataIndex: '',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <Link
                    to={{
                      pathname: '/TopicDetails',
                      state: { record },
                    }}
                  >
                    List Teacher
                  </Link>
              // <Button title="Sure?" onClick={() => this.handShow(record)}>
              //   <a>List Teacher</a>
              // </Button>
            ) : null,
        },

        {
          title: 'Edit',
          dataIndex: 'edit',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <Button title="Sure?" onClick={() => this.handEdit(record.id)}>
                <a>Edit</a>
              </Button>
            ) : null,
        },
        {
          title: 'Delete',
          dataIndex: 'operation',
          render: (text, record) =>
            this.state.dataSource.length >= 1 ? (
              <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
                <a>Delete</a>
              </Popconfirm>
            ) : null,
        },

      ]
    };

  }

  componentDidMount = async () => {
    let userId = localStorage.getItem('userId');
    let teacherId = localStorage.getItem('teacherId');
    let role = localStorage.getItem('bms-token');
    let roleUser = JSON.parse(role)
    let role1 =roleUser&&roleUser[0]
    await this.getAllTeacher()
    console.log(role1)
    if (role1 === "teacher") {
      console.log(userId)
      console.log(teacherId)
      await this.getTopicOfTeacher(teacherId);

      this.setState({
        columns: [
          {
            title: 'Topic Name',
            dataIndex: 'subName',
            key: 'id',
          },
          {
            title: 'Topic code',
            dataIndex: 'subID',
            key: 'id',
          },
        ]
      })
    }
    else {
      await this.getData()
    }
  }

  addItem = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };
  getTopicOfTeacher = async (id) => {
    const result = await getTopicOfTeacher(id);
    console.log(result)
    this.setState({ dataSource: result })
  }
  getAllTeacher = async () => {
    const result = await getAllTeacher();
    console.log(result)
    let teacherName = []
    result.map((item) => {
      teacherName.push(item.name)
    })
    console.log(teacherName)
    this.setState({ teacherNameList: teacherName })
  }
  getData = async () => {
    const result = await getAllTopic();
    console.log(result)
    this.setState({ dataSource: result })
  }
  getTopicDetails = async (id) => {
    const result = await getTopicDetails(id);
    console.log(result)
    this.setState({ dataModalEdit: result })
  }
  createTopic = async (values) => {
    console.log('Success:', values);
    let body = {
      "subName": values.topicName,
      "subID": values.topicID,
    }
    const result = await createTopic(body);
    console.log(result)
    await this.getData()
    this.setState({ modalVisible: false })
  };

  editTopic = async (values) => {
    let id = this.state.dataModalEdit.id
    console.log(id)
    console.log('Success:', values);
    let body = {
      "subName": values.topicName,
      "subID": values.topicID,
    }
    const result = await editTopic(body, this.state.dataModalEdit.id);
    console.log(result)
    await this.getData()
    this.setState({ modalVisibleEdit: false })
  };
  handleDelete = async (record) => {
    const result1 = await deleteTopic(record.id);
    console.log(result1)
    this.getData()
  };
  handEdit = async (id) => {
    console.log(id)
    await this.getTopicDetails(id)
    this.setState({ modalVisibleEdit: true })
  };
  handShow = async (record) => {
    console.log(record)
    this.setState({ listTeacher: record })
    this.setState({ modalListTeacher: true })
  };

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  render() {

    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    let columns1 = this.state.columns
    const columns = columns1.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    let teacherSelect=this.state.teacherNameList
    teacherSelect=["khanh","khanh1"]
    console.log(teacherSelect)

    return (
      <div>
        <PageHeader
          ghost={false}
          title={`Topic List`}
          extra={[
            <Button
              onClick={() => this.addItem()}
              type="primary"
              style={{
                marginBottom: 16,
              }}
            >
              Add Coures
          </Button>,
            <Button
              onClick={() => { this.getData() }}
              type="primary"
              style={{
                marginBottom: 16,
              }}

            >
              Reload
            </Button>,
          ]}
        ></PageHeader>

        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
        <Modal
          destroyOnClose
          title={this.state.listTeacher.subName}
          visible={this.state.modalListTeacher}
          onCancel={() => this.setState({ modalListTeacher: false })}
          footer={[
            <List
              size="large"
              bordered
              dataSource={this.state.listTeacher.teacherInfos}
              renderItem={item => <List.Item>{item.name}</List.Item>}
            />,
          ]}
        >
        </Modal>,
        <Modal
          destroyOnClose
          title="Add Topic"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              onFinish={this.createTopic}
            >
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Topic Name ',
                  },
                ]}

                label="Topic Name" name="topicName" >
                <Input placeholder="Enter Topic Name" />
              </Form.Item>
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Topic Code ',
                  },
                ]}

                label="Topic Code" name="topicID" >
                <Input placeholder="Enter Topic Code" />
              </Form.Item>
              {/* <Form.Item
                name="course"
                label="Course"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Course ',
                  },
                ]}
              >
                <Select placeholder="Course">
                  <Select value="Computing">Computing</Select>
                  <Select value="Development">Development</Select>
                </Select>
              </Form.Item> */}
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.closeModal()}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>

          ]}
        >
        </Modal>,
        <Modal
          destroyOnClose
          title="Edit Course Info"
          visible={this.state.modalVisibleEdit}
          onCancel={() => this.setState({ modalVisibleEdit: false })}
          footer={[

            <Form
              onFinish={this.editCourses}
            >
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Coures Name ',
                  },
                ]}
                label="Topic Name" name="subName" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.subName}>
                <Input placeholder="Topic Name" />
              </Form.Item>

              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Pleas enter Topic Name ',
                  },
                ]}
                label="Topic Code" name="subID" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.subID}>
                <Input placeholder="Topic Code" />
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>
          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
