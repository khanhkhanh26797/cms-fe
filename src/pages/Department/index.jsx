import { PlusOutlined } from '@ant-design/icons';
import { Button, Table, Card, Input, Drawer,PageHeader ,Descriptions} from 'antd';
import React, { useState, useEffect } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import { queryRule, updateRule, addRule, removeRule } from './service';
import { getAllDepartment } from '@/services/department';

const dataSource = [
  {
    authorities: null,
    created_by: "system",
    created_date: null,
    email: "admin@localhost",
    first_name: "Administrator",
    id: 1,
    last_modified_by: "system",
    last_modified_date: null,
    last_name: "Administrator",
    login: "admin",
  },
  {
    authorities: null,
    created_by: "system",
    created_date: null,
    email: "admin@localhost",
    first_name: "Administrator",
    id: 1,
    last_modified_by: "system",
    last_modified_date: null,
    last_name: "Administrator",
    login: "admin",
  },
];

const columns = [
  {
    title: 'Họ tên',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Tài khoản',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Phòng ban',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Email',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Giới tính',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Số điện thoại',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'name',
    key: 'id',
  },
  {
    title: 'Ngày cập nhập',
    dataIndex: 'name',
    key: 'id',
  },
];
const TableList = () => {
  const [data, setData] = useState([]);
  const [loading, setLoaing] = useState(false);
  // const fetchData = async () => {
  //   setLoaing(true)
  //   const result = await getAllDepartment() 
  //   setLoaing(false)
  //   setData(result.data);
  // };
  // useEffect(() => { 
  //   fetchData();
  // }, []);

  return (
    <> 
        <div className="site-page-header-ghost-wrapper">
    <PageHeader
      ghost={false} 
      title={`Danh sách nhân viên`} 
      extra={[
        <Button key="2" loading={loading}  size="small"  type="primary" onClick={() => fetchData()}>Thêm nhân viên</Button>, 
        <Button key="2" loading={loading}  size="small"  type="primary" onClick={() => fetchData()}>Reload</Button>, 
      ]}
    >

    </PageHeader>
  </div>,
      <Table dataSource={data} columns={columns} loading={loading}/>;
      {/* </Card> */}
    </>
  );
};

export default TableList;
