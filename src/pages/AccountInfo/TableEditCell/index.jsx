import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option } from 'antd';
import { getTeachInfoByUserId,editTeacherInfo } from '@/services/AccountInfo';
const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.state = {
      dataTeacher: {},
     modalVisible: false 
    };
  }

  componentDidMount = async () => {
    let userId = localStorage.getItem('userId');
    console.log(userId)
    await this.getTeachInfoByUserId(userId)
  }
  editInfo = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };
  getTeachInfoByUserId = async (id) => {
    const result = await getTeachInfoByUserId(id);
    console.log(result)
    this.setState({ dataTeacher: result[0] })
  }

  editItem = async (values) => {
    let userId = localStorage.getItem('userId');
    console.log(userId)
    console.log('Success:', values);
    let id = this.state.dataTeacher.id
    console.log(id)
    let body = {
      "name": values.name,
      "education": values.education,
      "phone": values.phone,
      "type": values.type,
      "workingPlace": values.workingPlace,

    }
    console.log(body)
    console.log(id)
    const result = await editTeacherInfo(body, id);
    console.log(result)
    await this.getTeachInfoByUserId(userId)
    this.setState({ modalVisible: false })
  };
  handleDelete = async (id) => {
    console.log(id)
    const result = await deleteUsers(id);
    console.log(result)

  };

  render() {
    console.log(this.state.dataTeacher)
    console.log(this.state.dataTeacherBase)
    return (
      <div>
        <div>
          
        </div>
        <Form
          {...formItemLayout}
          onFinish={this.editInfo}
          // form ={this.state.dataTeacher}
        >

          <Form.Item label="Teacher Name" name={this.state.dataTeacher &&this.state.dataTeacher.name} initialValue={this.state.dataTeacher && this.state.dataTeacher.name}>
            <Input />
          </Form.Item>

          <Form.Item label="Education" name={this.state.dataTeacher &&this.state.dataTeacher.education} initialValue={this.state.dataTeacher && this.state.dataTeacher.education} >
            <Input />
          </Form.Item>

          <Form.Item label="Working place" name={this.state.dataTeacher &&this.state.dataTeacher.workingPlace} initialValue={this.state.dataTeacher && this.state.dataTeacher.workingPlace}>
            <Input />
          </Form.Item>

          <Form.Item label="Phone" name={this.state.dataTeacher &&this.state.dataTeacher.phone} initialValue={this.state.dataTeacher && this.state.dataTeacher.phone}>
            <Input placeholder="" type="phone" />
          </Form.Item>

          <Form.Item
            name={this.state.dataTeacher &&this.state.dataTeacher.type}
            label="Type"
            hasFeedback
            initialValue={this.state.dataTeacher && this.state.dataTeacher.type}
          >
            <Select placeholder="Type">
              <Select value="External">External</Select>
              <Select value="Internal">Internal</Select>
            </Select>
          </Form.Item>
          <Form.Item
            style={{ alignItems: 'center', justifyContent: 'center' }}
          >
            <Button
              style={{ alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}
              type="primary"
              htmlType="submit"
            >
              Edit
      </Button>

          </Form.Item>
        </Form>
        <Modal
          destroyOnClose
          title="Update Profile"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              {...formItemLayout}
              onFinish={this.editItem}
            >
              <Form.Item label="Teacher Name" name="name" initialValue={this.state.dataTeacher && this.state.dataTeacher.name}>
                <Input  placeholder="" />
              </Form.Item>

              <Form.Item label="Education" name="education" initialValue={this.state.dataTeacher && this.state.dataTeacher.education} >
                <Input />
              </Form.Item>

              <Form.Item label="Working place" name="workingPlace" initialValue={this.state.dataTeacher && this.state.dataTeacher.workingPlace}>
                <Input />
              </Form.Item>

              <Form.Item label="Phone" name="phone" initialValue={this.state.dataTeacher && this.state.dataTeacher.phone}>
                <Input placeholder="" type="phone" />
              </Form.Item>

              <Form.Item
                name="type"
                label="Type"
                hasFeedback
                initialValue={this.state.dataTeacher && this.state.dataTeacher.type}
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Type of Teacher',
                  },
                ]}
              >
                <Select placeholder="Type">
                  <Select value="External">External</Select>
                  <Select value="Internal">Internal</Select>
                </Select>
              </Form.Item>
              <Form.Item
                style={{ alignItems: 'center', justifyContent: 'center' }}
              >
                <Button
                  style={{ alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}
                  type="primary"
                  htmlType="submit"
                >Update</Button>

              </Form.Item>
            </Form>

          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
