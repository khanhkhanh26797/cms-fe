import React, { useEffect } from 'react';
import { Form, Input, Select, Row, Col } from 'antd';
import { connect } from 'dva';
// import { isAdmin } from '@/constants/userRoles';
// import UploadAdapter from '@/pages/UploadAdapter';
import { getImageUrl } from '@/utils/utils';

const ProductForm = ({ form, categories, product = {}, role, vendors, dispatch }) => {
  /* eslint-disable-next-line no-unused-vars */
  const { getFieldDecorator } = form;

  // useEffect(() => {
  //   if (isAdmin(role.type)) {
  //     dispatch({
  //       type: 'vendor/fetchVendors',
  //     });
  //   }
  // }, []);

  const onUploadSuccess = image => form.setFieldsValue({ images: [+image.id] });

  return (
    <>

      <Row gutter={24}>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Name">
            {getFieldDecorator('name', {
              initialValue: product.name,
              rules: [{ required: true, message: 'Please fill in product name' }],
            })(<Input placeholder="Product name" />)}
          </Form.Item>
        </Col>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Variation">
            {getFieldDecorator('variation', {
              initialValue: product.variation,
              rules: [{ required: false, message: 'Please fill in product variation' }],
            })(<Input placeholder="Product variation" />)}
          </Form.Item>
        </Col>
      </Row>
      {/* <Row gutter={24}>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Category">
            {getFieldDecorator('category', {
              initialValue: product.category && product.category.id,
              rules: [{ required: true, message: 'Please select category' }],
            })(
              <Select placeholder="Select category">
                {categories.map(category => (
                  <Select.Option value={category.id}>{category.name}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Price">
            {getFieldDecorator('price', {
              initialValue: product.price,
              rules: [{ required: true, message: 'Please input price' }],
            })(<Input placeholder="Price" type="number" />)}
          </Form.Item>
        </Col>
      </Row> */}
      {/* <Row gutter={24}>
        <Col md={{ span: 12 }}>
          <Form.Item label="Product Reference">
            {getFieldDecorator('sku', {
              initialValue: product.sku,
              rules: [{ required: true, message: 'Please input SKU' }],
            })(<Input placeholder="SKU" />)}
          </Form.Item>
        </Col>
    
      </Row> */}
      {/* <Row gutter={24}>


        {isAdmin(role.type) && vendors && (
          <Col md={{ span: 12 }}>
            <Form.Item label="Product Provider">
              {getFieldDecorator('vendor', {
                initialValue: product.vendor && product.vendor.id,
                rules: [{ required: false, message: 'Please input provider' }],
              })(
                <Select placeholder="Select vendor">
                  {vendors.map(vendor => (
                    <Select.Option value={vendor.id} key={vendor.id}>
                      {vendor.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          </Col>
        )}
      </Row> */}
      <Row>
        <Form.Item label="Product description">
          {getFieldDecorator('description', {
            initialValue: product.description,
          })(<Input.TextArea placeholder="Describe your product" rows={4} />)}
        </Form.Item>
      </Row>
    </>
  );
};

export default connect(({ user, vendor }) => ({
  // role: user.currentUser.role,
  // vendors: vendor.list,
}))(ProductForm);
