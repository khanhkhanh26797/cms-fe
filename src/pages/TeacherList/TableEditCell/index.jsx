import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option } from 'antd';

import { getTeacherDetails, getAllTeacher, createUsers, createTeacherInfo, editTeacherInfo, deleteUsers, deleteTeacherInfo } from '@/services/TeacherList';

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};
const formItemLayout = {
  labelCol: {
    xs: {
      span: 12,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 12,
    },
  },
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
  }

  return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.columns = [
      {
        title: 'Account',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Teacher Name',
        dataIndex: 'name',
        key: 'id',
      },
      {
        title: 'Education',
        dataIndex: 'education',
        key: 'id',
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'id',
      },
      {
        title: 'Phone',
        dataIndex: 'phone',
        key: 'id',
      },
      {
        title: 'Working place',
        dataIndex: 'workingPlace',
        key: 'id',
      },
      {
        title: 'Edit',
        dataIndex: 'edit',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Button title="Sure?" onClick={() => this.handEdit(record.id)}>
              <a>Edit</a>
            </Button>
          ) : null,
      },
      {
        title: 'Delete',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      form: [],
      dataSource: [],
      count: 2,
      modalVisible: false,
      modalVisibleEdit: false,
      formInput: null,
      first_name: null,

      // dataModalEdit: {
      //   authorities: [
      //     1
      //   ],
      //   department_id: 1,
      //   first_name: "khanh",
      //   last_name: "khanh",
      //   login: "khanh",
      //   email: "khanh",
      //   phone: "khanh",
      // },
      dataModalEdit: null
    };

  }

  componentDidMount = async () => {
    await this.getData()

    // this.textInput.current.focusTextInput();
  }

  addItem = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };

  getData = async () => {
    const result = await getAllTeacher();
    console.log(result)
    this.setState({ dataSource: result })
  }

  getTeacherDetails = async (id) => {
    const result = await getTeacherDetails(id);
    console.log(result)
    this.setState({ dataModalEdit: result })

  }
  addTeacher = async (values) => {
    console.log('Success:', values);
    let body = {
      "username": values.name,
      "email": values.email,
      "password": "12345678",
      "role": "teacher"
    }

    const result = await createUsers(body);
    console.log(result)
    await this.createTeacherInfo(values, result.user.id, result.user.email)
    await this.getData()
    this.setState({ modalVisible: false })
  };

  createTeacherInfo = async (values, idUser, email) => {
    console.log('Success:', values);
    let body = {
      "name": values.name,
      "education": values.education,
      "phone": values.phone,
      "type": values.type,
      "workingPlace": values.workingPlace,
      "email": email,
      "user": idUser
    }
    console.log(body)
    const result = await createTeacherInfo(body);
    console.log(result)
    this.setState({ modalVisible: false })
  };
  editTeacherInfo = async (values) => {
    let id = this.state.dataModalEdit.id
    console.log(id)
    console.log('Success:', values);
    let body = {
      "name": values.name,
      "education": values.education,
      "phone": values.phone,
      "type": values.type,
      "workingPlace": values.workingPlace,

    }
    const result = await editTeacherInfo(body, this.state.dataModalEdit.id);
    console.log(result)
    this.setState({ modalVisibleEdit: false })
  };
  handleDelete = async (record) => {
    console.log(record)
    let userId = record.user.id
    const result = await deleteUsers(userId);
    console.log(result)
    const result1 = await deleteTeacherInfo(record.id);
    console.log(result1)
    this.getData()
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };
  handEdit = async (id) => {
    console.log(id)
    await this.getTeacherDetails(id)

    this.setState({ modalVisibleEdit: true })
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  render() {

    console.log(this.state.dataModalEdit)
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>

        <PageHeader
          ghost={false}
          title={`Teacher List`}
          extra={[
            <Button
              onClick={() => this.addItem()}
              type="primary"
              style={{
                marginBottom: 16,
              }}
            >
              Add Teacher
          </Button>,
            <Button
              onClick={() => { this.getData() }}
              type="primary"
              style={{
                marginBottom: 16,
              }}

            >
              Reload
            </Button>,
          ]}
        ></PageHeader>

        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
        <Modal
          destroyOnClose
          title="Add Student"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              {...formItemLayout}
              onFinish={this.addTeacher}
            >
              <Form.Item label="Account" name="email" >
                <Input type="email" placeholder="Enter account" />
              </Form.Item>

              <Form.Item label="Teacher Name" name="name">
                <Input placeholder="Teacher Name" />
              </Form.Item>

              <Form.Item label="Education" name="education">
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Working place" name="workingPlace">
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Phone" name="phone">
                <Input placeholder="" type='number' />
              </Form.Item>

              <Form.Item
                name="type"
                label="Type"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Type of Teacher',
                  },
                ]}
              >
                <Select placeholder="Type">
                  <Select value="External">External</Select>
                  <Select value="Internal">Internal</Select>
                </Select>
              </Form.Item>
              <Form.Item style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.closeModal()}>
                  Huỷ
              </Button>
              </Form.Item>
            </Form>

          ]}
        >
        </Modal>,
        {/* <ModalStaff visible={this.state.modalVisibleEdit} data={this.state.modalVisibleEdit}></ModalStaff> */}
        <Modal
          destroyOnClose
          title="Edit Student Info"
          visible={this.state.modalVisibleEdit}
          onCancel={() => this.setState({ modalVisibleEdit: false })}
          footer={[

            <Form
              {...formItemLayout}
              onFinish={this.editTeacherInfo}
            // form = {this.prop.dataModalEdit}
            >
              <Form.Item label="Teacher Name" name="name" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.name}>
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Education" name="education" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.education} >
                <Input />
              </Form.Item>

              <Form.Item label="Working place" name="workingPlace" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.workingPlace}>
                <Input />
              </Form.Item>

              <Form.Item label="Phone" name="phone" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.phone}>
                <Input placeholder="" type="phone" />
              </Form.Item>

              <Form.Item
                name="type"
                label="Type"
                hasFeedback
                initialValue={this.state.dataModalEdit && this.state.dataModalEdit.type}
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Type of Teacher',
                  },
                ]}
              >
                <Select placeholder="Type">
                  <Select value="External">External</Select>
                  <Select value="Internal">Internal</Select>
                </Select>
              </Form.Item>
              <Form.Item style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
                  Huỷ
        </Button>
              </Form.Item>
            </Form>
          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
