import { PlusOutlined } from '@ant-design/icons';
import { Button, Table, Card, Input, Drawer, PageHeader, Descriptions, Modal } from 'antd';
import React, { useState, useEffect, useCallback } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import { queryRule, updateRule, addRule, removeRule } from './service';
import { getAllUsers } from '@/services/StudenList';
import TableEditCell from './TableEditCell';
const dataSource = [
  {
    authorities: null,
    created_by: 'system',
    created_date: null,
    email: 'admin@localhost',
    first_name: 'Administrator',
    id: 1,
    last_modified_by: 'system',
    last_modified_date: null,
    last_name: 'Administrator',
    login: 'admin',
  },
  {
    authorities: null,
    created_by: 'system',
    created_date: null,
    email: 'admin@localhost',
    first_name: 'Administrator',
    id: 1,
    last_modified_by: 'system',
    last_modified_date: null,
    last_name: 'Administrator',
    login: 'admin',
  },
];
const columns = [
  {
    title: 'Họ tên',
    dataIndex: 'first_name',
    key: 'id',
  },
  {
    title: 'Tài khoản',
    dataIndex: 'first_name',
    key: 'id',
  },
  {
    title: 'Phòng ban',
    dataIndex: 'first_name',
    key: 'id',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'id',
  },
  {
    title: 'Giới tính',
    dataIndex: 'email',
    key: 'id',
  },
  {
    title: 'Số điện thoại',
    dataIndex: 'email',
    key: 'id',
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'email',
    key: 'id',
  },
  {
    title: 'Ngày cập nhập',
    dataIndex: 'email',
    key: 'id',
  },
];

const TableList = () => {
  const [data, setData] = useState([]);
  const [loading, setLoaing] = useState(false);
  const [createModalVisible, handleModalVisible] = useState(false);


  return (
    <>
      <div className="site-page-header-ghost-wrapper">
      </div>
      <TableEditCell />
    
    </>
  );
};

export default TableList;
