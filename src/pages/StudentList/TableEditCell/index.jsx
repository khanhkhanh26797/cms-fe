import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option } from 'antd';
import { getAllStudents,createStudentInfo,editStudentInfo,deleteStudentInfo, createUsers, getUsersDetails, editUsers, deleteUsers } from '@/services/StudenList';
const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditForm = ({ index, ...props }) => {
  const { getFieldDecorator } = form;
  return (
    <Form
    // onFinish={this.props.onFinish}
    // form={this.state.dataModalEdit}
    >
      <Form.Item label="Họ và tên đệm" name="first_name">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Tên nhân viên" name="last_name">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Tên đăng nhập" name="login">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Email đăng nhập" name="email">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Số điện thoại" name="phone">
        <Input placeholder="" type="phone" />
      </Form.Item>
      <Form.Item
        name="authorities"
        label="Select"
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Chọn loại tài khoản trước khi tạo!',
          },
        ]}
      >
        <Select placeholder="Chọn loại tài khoản">
          <Select value="0">Quản lý</Select>
          <Select value="1">Role 2</Select>
          <Select value="2">Trưởng nhóm</Select>
          <Select value="3">Nhân viên</Select>
        </Select>
      </Form.Item>
      <Form.Item >
        <Button
          type="primary"
          htmlType="submit"
        >
          Thêm
              </Button>
        <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
          Huỷ
        </Button>,
          </Form.Item>
    </Form>
  );
};
function CustomTextInput(props) {
  return (
    <div>
      <input ref={props.inputRef} />
    </div>
  );
}

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
  }

  return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.columns = [
      {
        title: 'Account',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Student Name',
        dataIndex: 'name',
        key: 'id',
      },
      {
        title: 'Age',
        dataIndex: 'age',
        key: 'id',
      },
      {
        title: 'Date of birth',
        dataIndex: 'dob',
        key: 'id',
      },
      {
        title: 'Education',
        dataIndex: 'education',
        key: 'id',
      },
      {
        title: 'Department',
        dataIndex: 'department',
        key: 'id',
      },
      {
        title: 'Toeic Score',
        dataIndex: 'toeicScore',
        key: 'id',
      },
      {
        title: 'Location',
        dataIndex: 'location',
        key: 'id',
      },
      {
        title: 'Edit',
        dataIndex: 'edit',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure?" onConfirm={() => this.handEdit(record.id)}>
              <a>Edit</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Delete',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      form: [],
      dataSource: [],
      count: 2,
      modalVisible: false,
      modalVisibleEdit: false,
      dataModalEdit: null
    };

  }

  componentDidMount = async () => {
    await this.getData()

    // this.textInput.current.focusTextInput();
  }

  addItem = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };

  getData = async () => {
    const result = await getAllStudents();
    console.log(result)
    this.setState({ dataSource: result })
  }
  getUsersDetails = async (id) => {
    const result = await getUsersDetails(id);
    console.log(result)
    this.setState({ dataModalEdit: result })
    // this.setState({ first_name: result.data.first_name })

    // this.props.dataModalEdit.setFieldsValue({
    //   first_name: result.data.first_name,
    // })
  }
  addStudent = async (values) => {
    console.log('Success:', values);
    let body = {
      "username": values.name,
      "email": values.email,
      "password": "12345678",
      "role": "student"
    }

    const result = await createUsers(body);
    console.log(result)
    await this.createStudentInfo(values,result.user.id,result.user.email)
    await this.getData()
    this.setState({ modalVisible: false })
  };

  createStudentInfo = async (values,idUser,email) => {
    console.log('Success:', values);
    let body = {
      "name": values.name,
      "age": values.age,
      "education": values.education,
      "toeicScore": values.toeicScore,
      "department": values.department,
      "location": values.location,
      "email": email,
      "user": idUser
    }
    console.log(body)
    const result = await createStudentInfo(body);
    console.log(result)
    this.setState({ modalVisible: false })
  };
  editStudentInfo = async (values) => {
    let id =this.state.dataModalEdit.id
    console.log(id)
    console.log('Success:', values);
    let body = {
      "name": values.name,
      "age": values.age,
      "education": values.education,
      "toeicScore": values.toeicScore,
      "department": values.department,
      "location": values.location,
      "email": values.email,
    }
    const result = await editStudentInfo(body, this.state.dataModalEdit.id);
    console.log(result)
    this.setState({ modalVisibleEdit: false })
  };
  handleDelete = async (record) => {
    console.log(record)
    let userId = record.user.id
    const result = await deleteUsers(userId);
    console.log(result)
    const result1 = await deleteStudentInfo(record.id);
    console.log(result1)
    this.getData()
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };
  handEdit = async (id) => {
    console.log(id)
    await this.getUsersDetails(id)
    this.setState({ modalVisibleEdit: true })
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };

  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  render() {

    console.log(this.state.dataModalEdit)
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>

        <PageHeader
          ghost={false}
          title={`Student List`}
          extra={[
            <Button
              onClick={() => this.addItem()}
              type="primary"
              style={{
                marginBottom: 16,
              }}
            >
              Add student
          </Button>,
            <Button
              onClick={() => { this.getData() }}
              type="primary"
              style={{
                marginBottom: 16,
              }}

            >
              Reload
            </Button>,
          ]}
        ></PageHeader>

        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
        <Modal
          destroyOnClose
          title="Add Student"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              onFinish={this.addStudent}
            >
              <Form.Item label="Account" name="email" >
                <Input type="email" placeholder="Enter account" />
              </Form.Item>

              <Form.Item label="Student Name" name="name">
                <Input placeholder="Student Name" />
              </Form.Item>

              <Form.Item label="Age" name="age">
                <Input placeholder="Age" />
              </Form.Item>

              <Form.Item label="Education" name="education">
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Location" name="location">
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Toeic Score" name="toeicScore">
                <Input placeholder="" type='number' />
              </Form.Item>

              <Form.Item
                name="department"
                label="Department"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Pleas choose Department ',
                  },
                ]}
              >
                <Select placeholder="Department">
                  <Select value="IT">Information Technology</Select>
                  <Select value="Business">Business</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.closeModal()}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>

          ]}
        >
        </Modal>,
        {/* <ModalStaff visible={this.state.modalVisibleEdit} data={this.state.modalVisibleEdit}></ModalStaff> */}
        <Modal
          destroyOnClose
          title="Edit Student Info"
          visible={this.state.modalVisibleEdit}
          onCancel={() => this.setState({ modalVisibleEdit: false })}
          footer={[

            <Form
              onFinish={this.editStudentInfo}
            // form = {this.prop.dataModalEdit}
            >
              <Form.Item label="Email " name="email" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.email}>
                <Input placeholder="" type="email" />
              </Form.Item>

              <Form.Item label="Student Name" name="name" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.name} >
                <Input/>
              </Form.Item>

              <Form.Item label="Age" name="age" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.age}>
                <Input />
              </Form.Item>
              
              <Form.Item label="Education" name="education" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.education}>
                <Input placeholder="" type="phone" />
              </Form.Item>

              <Form.Item
                name="department"
                label="Department"
                hasFeedback
                initialValue={this.state.dataModalEdit && this.state.dataModalEdit.department}
                rules={[
                  {
                    required: true,
                    message: 'Chọn loại tài khoản trước khi tạo!',
                  },
                ]}
              >
                <Select placeholder="Department">
                  <Select value="IT">Information Technology</Select>
                  <Select value="Business">Business</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>
          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
