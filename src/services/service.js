import request from '@/utils/request';
import { stringify } from 'qs';
export async function getAllServices() {
  return request('/services', {
    method: 'GET',
  });
}
export async function getServicesDetails(id) {
  return request(`/services/${stringify(id)}`, {
      method: 'GET',
    });
  } 

  export async function createServices(params) {
    return request('/services', {
      method: 'POST',
      data: params,
    });
  } 
  export async function editServices(params,id) {
    return request(`/services/${stringify(id)}`, {
      method: 'PUT',
      data: params,
    });
  }
  // export async function deleteServices(params) {
  //   return request('/services', {
  //     method: 'DELETE',
  //     data: params,
  //   });
  // }
