import request from '@/utils/request';
import { stringify } from 'qs';

export async function getTeachInfoByUserId(id) {
  return request(`/teacher-infos/?user=${id}`, {
    method: 'GET',
  });
}
export async function editTeacherInfo(data,id) {
  return request(`/teacher-infos/${id}` , {
    method: 'PUT',
    data
  });
}
