import request from '@/utils/request';
import { stringify } from 'qs';
// export async function getAllUsers() {
//   return request('/users', {
//     method: 'GET',
//   });
// }
export async function getAllTopic() {
  return request('/subjects', {
    method: 'GET',
  });
}
export async function getAllTeacher() {
  return request('/teacher-infos', {
    method: 'GET',
  });
}

export async function createTopic(data) {
  return request('/subjects', {
    method: 'POST',
    data
  });
}
export async function editTopic(data, id) {
  return request(`/subjects/${id}`, {
    method: 'PUT',
    data
  });
}
export async function getTopicDetails(id) {
  return request(`/subjects/${id}`, {
    method: 'GET',
  });
}
export async function getTopicOfTeacher(id) {
  return request(`/subjects/?teacherInfos.id=${id}`, {
    method: 'GET',
  });
}

export async function deleteTopic(id) {
  return request(`/subjects/${id}`, {
    method: 'DELETE',
  });
}
//
export async function getUsersDetails(id) {
  return request(`/student-infos/${id}`, {
    method: 'GET',
  });
}

export async function createUsers(data) {
  return request('/auth/local/register', {
    method: 'POST',
    data
  });
}
export async function editUsers(data, id) {
  return request(`/users/${id}`, {
    method: 'PUT',
    data
  });
}
export async function deleteUsers(id) {
  return request(`/users/${id}`, {
    method: 'DELETE',
  });
}