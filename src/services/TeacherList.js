import request from '@/utils/request';
import { stringify } from 'qs';
// export async function getAllUsers() {
//   return request('/users', {
//     method: 'GET',
//   });
// }
export async function getAllTeacher() {
  return request('/teacher-infos', {
    method: 'GET',
  });
}
export async function createUsers(data) {
  return request('/auth/local/register', {
    method: 'POST',
    data
  });
}
export async function createTeacherInfo(data) {
  return request('/teacher-infos', {
    method: 'POST',
    data
  });
}
export async function editTeacherInfo(data,id) {
  return request(`/teacher-infos/${id}` , {
    method: 'PUT',
    data
  });
}
export async function deleteTeacherInfo(id) {
  return request(`/teacher-infos/${id}`, {
    method: 'DELETE',
  });
}
export async function getTeacherDetails(id) {
  return request(`/teacher-infos/${id}` , {
    method: 'GET',
  });
}
export async function editUsers(data,id) {
    return request(`/users/${id}`, {
      method: 'PUT',
      data
    });
  }
  export async function deleteUsers(id) {
    return request(`/users/${id}`, {
      method: 'DELETE',
    });
  }