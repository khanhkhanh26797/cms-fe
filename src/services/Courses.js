import request from '@/utils/request';
import { stringify } from 'qs';
// export async function getAllUsers() {
//   return request('/users', {
//     method: 'GET',
//   });
// }
export async function getAllCourses() {
  return request('/courses', {
    method: 'GET',
  });
}

export async function createCourses(data) {
  return request('/courses', {
    method: 'POST',
    data
  });
}

export async function editCourses(data,id) {
  return request(`/courses/${id}`, {
    method: 'PUT',
    data
  });
}
export async function getCoursesDetails(id) {
  return request(`/courses/${id}`, {
      method: 'GET',
    });
  } 
  export async function deleteCourses(id) {
    return request(`/courses/${id}`, {
      method: 'DELETE',
    });
  }
//

export async function getUsersDetails(id) {
  return request(`/student-infos/${id}`, {
      method: 'GET',
    });
} 


export async function createUsers(data) {
    return request('/auth/local/register', {
      method: 'POST',
      data
    });
  }

  export async function editUsers(data,id) {
    return request(`/users/${id}`, {
      method: 'PUT',
      data
    });
  }
  export async function deleteUsers(id) {
    return request(`/users/${id}`, {
      method: 'DELETE',
    });
  }
