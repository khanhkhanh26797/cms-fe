import request from '@/utils/request';
import { stringify } from 'qs';
// export async function getAllUsers() {
//   return request('/users', {
//     method: 'GET',
//   });
// }
export async function getAllStudents() {
  return request('/student-infos', {
    method: 'GET',
  });
}

export async function createStudentInfo(data) {
  return request('/student-infos', {
    method: 'POST',
    data
  });
}
export async function editStudentInfo(data,id) {
  return request(`/student-infos/${id}`, {
    method: 'PUT',
    data
  });
}
export async function deleteStudentInfo(id) {
  return request(`/student-infos/${id}`, {
    method: 'DELETE',
  });
}

export async function getUsersDetails(id) {
  return request(`/student-infos/${id}`, {
      method: 'GET',
    });
  } 


export async function createUsers(data) {
    return request('/auth/local/register', {
      method: 'POST',
      data
    });
  }

  export async function editUsers(data,id) {
    return request(`/users/${id}`, {
      method: 'PUT',
      data
    });
  }
  export async function deleteUsers(id) {
    return request(`/users/${id}`, {
      method: 'DELETE',
    });
  }
